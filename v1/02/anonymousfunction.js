const f = function () {
    return function (x, y) {
        if (x-y > 0) {
            return 1
        }
        else if (x-y < 0) {
            return -1
        }
        else
            return 0;
    }
}();

let tulos = f(3, -2);

console.log(tulos);