const f = function () {
    return function (x, y) {
        return x-y;
    }
}();

const f2 = function (f, year2015, year2016) {
    let result = 0;
    for (let i = 0; i < 12; i++) {
        if (f(year2015[i], year2016[i]) < 0) {
            result++;
        }
    }
    return result
};

let tulos = f2(f, [-10,-3,-1,4,5,12,20,18,8,7,3,3], [-8,-4,0,4,4,10,12,18,15,9,7,3]);

console.log(tulos);