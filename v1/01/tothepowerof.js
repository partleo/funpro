console.log(power = ((p, q) => {
    let result = p;
    if (q === 0) {
        return 1
    }
    else if (q < 0) {
        result = 1/p;
        q *= -1;
        for (let i = 1; i < q; i++) {
            result = result*(1/p);
        }
    }
    else {
        for (let i = 1; i < q; i++) {
            result = result*p;
        }
    }
    return result
})(2, -3));

const power2 = (p, q) => {
    if (q === 0)
        return 1;
    else if (q < 0) {
        return 1/p * power2(p, q + 1);
    }
    else
        return p * power2(p, q - 1);
};

console.log(power2(3, -2));