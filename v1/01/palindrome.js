console.log(isPalindrome = ((string) => {
    const length = string.length;
    if (length * length === length) {
        return true
    }
    else if (string[0] !== string[length - 1]) {
        return false
    }
    else {
        const middle = string.slice(1, -1);
        return middle === middle.split('').reverse().join('');
    }
})("imaami"));