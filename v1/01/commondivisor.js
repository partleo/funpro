console.log(syt = ((p, q) => {
    if (q === 0) {
        return p
    }
    p = Math.abs(p);
    q = Math.abs(q);
    while(q) {
        const t = q;
        q = p % q;
        p = t;
    }
    return p;
})(20, 8));



console.log(kjl = ((p, q) => {
    if (p === 0 || q === 0) {
        return false
    }
    else if (p === 1 || q === 1) {
        return true
    }
    p = Math.abs(p);
    q = Math.abs(q);
    while(q) {
        const t = q;
        q = p % q;
        p = t;
    }
    return p !== 1;
})(6, 2));



const syt2 = (p, q) => {
    if (q === 0) {
        return p
    }
    if (!q) {
        return p;
    }
    else {
        const t = q;
        q = p % q;
        p = t;
        return syt2(p, q);
    }
};

const kjl2 = (p, q) => {
    if (p === 0 || q === 0) {
        return false
    }
    else if (p === 1 || q === 1) {
        return true
    }
    return Number.isInteger(p / q) || Number.isInteger(q / p);
};

console.log(syt2(8, 4));
console.log(kjl2(3, 7));