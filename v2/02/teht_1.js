function laskePisteet(pituus, kPiste, lisapisteet){
    if(pituus>=kPiste){
        return 60 + ((pituus-kPiste)*lisapisteet);
    }else if(pituus<kPiste){
        return ((pituus-kPiste)*(lisapisteet));
    }
}

function currying(kPiste, lisapisteet){
    return function(pituus){
        return laskePisteet(pituus, kPiste, lisapisteet);
    };
}

const normaaliLahti = currying(80 ,2.0);
const suurmäkiLahti = currying(120 ,1.8);

console.log(normaaliLahti(81));
console.log(suurmäkiLahti(121));