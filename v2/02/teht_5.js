const Immutable = require('immutable');

const isNotFactorOf = a => b => a % b !== 0;

const isPalindrome = x => {
    if (x.length < 2) {
        return true;
    }
    if (x[0] === x[x.length - 1]) {
        return isPalindrome(x.slice(1, -1));
    }
    else {
        return false;
    }
};

const isPrime = n => {
    return Immutable.Range(2, Math.floor(Math.sqrt(n)) + 1)
        .every(isNotFactorOf(n));
};

const primes = Immutable.Range(2).filter(isPrime);
const palindromePrimes = primes.filter(n => isPalindrome(n.toString()));

console.log(palindromePrimes.take(10).toArray());
