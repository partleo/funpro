const Immutable = require('immutable');
const set1 = Immutable.Set(['punainen', 'vihreä', 'keltainen']);

set2 = set1 & 'ruskea';

console.log("set1: " + set1 + ", set2: " + set2);
console.log(set1 === set2); //return false

set2 = set2 & 'ruskea';
set3 = set2 & 'musta';

console.log("set2: " + set2 + ", set3: " + set3);
console.log(set2 === set3); //return true