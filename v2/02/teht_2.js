const Auto  = (function(){

    const wm = new WeakMap();

    class Auto{
        constructor(p_tankki, p_matkamittari){
            wm.set(this, {matkamittari: p_matkamittari});
            this.tankki = p_tankki;
        }
        getMatkamittari() {
            return wm.get(this).matkamittari;
        }
        getTankki() {
            return this.tankki;
        }
        aja() {
            wm.get(this).matkamittari++;
            this.tankki--;
        }
        ajaMäärä(x) {
            wm.get(this).matkamittari+=x;
            this.tankki-=x;
        }
        tankkaa() {
            this.tankki++;
        }
        tankkaaMäärä(x) {
            this.tankki += x;
        }
    }
    return Auto;
})();

const auto = new Auto(30, 0 );
auto.aja();
auto.tankkaa();
auto.ajaMäärä(15);
auto.tankkaaMäärä(5);
console.log("Ajettu määrä: " + auto.getMatkamittari());
console.log("Polttoaineen määrä :" + auto.getTankki());
