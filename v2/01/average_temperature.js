const averageTemperatures = [-10,-3,-1,4,5,12,20,18,8,7,3,3,-8,-4,0,4,4,10,12,18,15,9,7,3];

const getTemperatures = (average) => {
    return average;
};

const positiveTemperatures = averageTemperatures.filter(average => average > 0)
    .map(getTemperatures);
const averagePositiveTemperatures = positiveTemperatures
    .reduce((total, temperature) => total + temperature) / positiveTemperatures.length;

console.log(averagePositiveTemperatures);