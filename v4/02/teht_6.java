import java.text.Normalizer;
import java.util.function.Function;
import java.util.function.UnaryOperator;

public class teht_6 {
    public static void main(String[] args) {
        UnaryOperator<String> removeUnnecessarySpaces = src ->
                src.replaceAll(" +", " ");
        
        UnaryOperator<String> removeUmlauts = src ->
                Normalizer.normalize(src, Normalizer.Form.NFD)
                .replaceAll("[^\\p{ASCII}]", "");
        
        UnaryOperator<String> orthography = src -> 
                src.toLowerCase()
                .replaceAll("sturct", "struct");
        
        Function<String, String> chain = removeUnnecessarySpaces
                .andThen(removeUmlauts)
                .andThen(orthography);

        System.out.println(chain.apply("ÅÄÖÜåäöü    Sturct  sturct"));
    }
}
