(def averagetemperatures [[-5.0 -3.3 1.4 3.0 6.9 13.2 12.2 14.9 9.5 7.4 3.5 1.0]
                          [-8.5 -2.0 2.2 4.3 6.5 10.9 15.2 13.2 10.0 8.2 3.5 1.4]])

(defn average [x]
  (/ (reduce + x) (count x)))

(defn transpose [x]
  (apply map vector x))

(def avg #(/ (reduce + %) (count %)))


(def data (map average (transpose averagetemperatures)))

(avg (remove neg? data))
