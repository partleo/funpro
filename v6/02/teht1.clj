(defn f "allekirjoitus" [& args] (apply str args))

(def formalFinnish (partial f "Ystävällisin terveisin, "))
(def formalEnglish (partial f "Sincerely, "))
(def informalFinnish (partial f "T. "))
(def informalEnglish (partial f "Regards, "))

(formalFinnish "Leo Partanen")
(informalFinnish "Leo")
