import java.util.function.DoubleUnaryOperator;

public class Makihyppy {

    static DoubleUnaryOperator makePistelaskuri(double kPiste, double lisapisteet){
        return (double pituus) -> {
            double pisteet = 60;
            if(pituus >= kPiste){
                return pisteet += (pituus-kPiste) * lisapisteet;
            }
            else{
                return pisteet -= (kPiste-pituus) * lisapisteet;
            }
        };
    }

    public static void main(String[] args) {
       DoubleUnaryOperator normaaliLahti = makePistelaskuri(80, 2.0);
       System.out.println(normaaliLahti.applyAsDouble(100));
    }
}