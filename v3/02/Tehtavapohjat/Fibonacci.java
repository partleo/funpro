
import java.util.function.IntSupplier;
import java.util.stream.IntStream;

public class Fibonacci {
    public static void main(String[] args) {
        IntSupplier fibo = new IntSupplier() {
            private int edellinen = 0;
            private int nykynen = 1;
            private int seuraava = 0;
            
            @Override
            public int getAsInt() {
                if (this.seuraava == 0) {
                    this.seuraava = 1;
                    return 0;
                }
                else  {
                    this.seuraava = this.edellinen + this.nykynen;
                    this.edellinen = this.nykynen;
                    this.nykynen = seuraava;
                    return this.edellinen;
                }
            }
        };
        System.out.print("Fibonaccin luvut:");
        IntStream.generate(fibo).limit(10).forEach((n) -> System.out.print(" " + n));
    }
}