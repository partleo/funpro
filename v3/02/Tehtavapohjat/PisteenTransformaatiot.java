import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Function;
import java.util.function.UnaryOperator;

public class PisteenTransformaatiot {
          
    public static void main(String[] args) {
               
       Function siirto = Piste.makeSiirto(1, 2);
       Function skaalaus = Piste.makeSkaalaus(2);
       Function kierto = Piste.makeKierto(Math.PI);
       Function muunnos = siirto.andThen(skaalaus).andThen(kierto);
       
       Piste[] pisteet = {new Piste(1,1), new Piste(2,2), new Piste(3,3)};
       List<Piste> uudetPisteet = new CopyOnWriteArrayList();
       
       for (Piste p: pisteet){
           uudetPisteet.add((Piste)muunnos.apply(p));
       } 
  
       uudetPisteet.forEach(p -> System.out.println(p));
    }
}

class Piste {
    private double x;
    private double y;

    Piste(double x, double y) {
        this.x = x;
        this.y = y;
    }

    static UnaryOperator<Piste> makeKierto(double r) {
        return p -> new Piste(p.getX() * Math.cos(r) - p.getY() * Math.sin(r),
                              p.getY() * Math.sin(r) + p.getY() * Math.cos(r));
    }

    static UnaryOperator<Piste> makeSiirto(double dx, double dy) {
        return p -> new Piste(p.getX() + dx, p.getY() + dy);
    }

    static UnaryOperator<Piste> makeSkaalaus(double n) {
        return p -> new Piste(p.getX() * n, p.getY() * n);
    }

    double getY() {
        return y;
    }

    double getX() {
        return x;
    }

    @Override
    public String toString() {
        return String.format("(%.1f; %.1f)", this.getX(), this.getY());
    }
}
