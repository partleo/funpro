
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Lottery {
    
    int amount = 7;
    final Random random = new Random();
    List<Integer> a = new ArrayList<>();
    
    public static void main(String[] args) {
                
        Lottery lottery = new Lottery();
        
        lottery.getLotteryNumbers();
        lottery.getLotteryNumbersLambda();
        lottery.getLotteryNumbersInnerClass();
        
    }
    
    public Lottery(){
        
    }
    
    public void getLotteryNumbers(){
        IntStream.of(random.ints(1, 40)
                .distinct()
                .limit(amount)
                .sorted()
                .toArray())
                .forEach((n) -> System.out.print(n + " "));            
    }
    
    public void getLotteryNumbersLambda(){
        IntStream.generate(() -> (int) (Math.random() * 39 + 1))
                .limit(amount)
                .boxed()
                .collect(Collectors.groupingBy(num -> num, 
                 Collectors.counting()))
                .forEach((Integer num, Long count) -> {
                    a.add(num);
                    if (a.size() == amount) {
                        Collections.sort(a);
                        System.out.println(a);
                    }
        });          
    }
    
    public void getLotteryNumbersInnerClass(){
        IntStream.generate(() -> (int) (Math.random() * 39 + 1))
                .limit(amount)
                .boxed()
                .collect(Collectors.groupingBy(num -> num, 
                 Collectors.counting()))
                .forEach(new BiConsumer<Integer, Long>() {
            @Override
            public void accept(Integer num, Long count) {
                a.add(num);
                if (a.size() == amount) {
                    Collections.sort(a);
                    System.out.println(a);
                }
            }
        });          
    }
}