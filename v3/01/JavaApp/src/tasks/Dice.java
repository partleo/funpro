
import java.util.stream.Collectors;
import java.util.stream.Stream;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author leopa
 */
public class Dice {
    
    int totalCount;
    
    public Dice(){
        
    }
    
    public void rollDice(){
        Stream.generate(() -> (int) (Math.random() * 6 + 1))
             .limit(20)
             .collect(Collectors.groupingBy(num -> num, 
                 Collectors.counting()))
             .forEach((Integer num, Long count) -> {
                 if (num == 6) {
                     System.out.println("number " + num + " was rolled " + count + " times");
                 }
                 else {
                     totalCount+=count;
                     if (totalCount == 20) {
                         System.out.println("number 6 was rolled 0 times");
                     }
                 }
                 
                 
                 
        });
    }
    
    
    public static double toDouble(int i){
        double d = i;
        return d;
    }
      
}
