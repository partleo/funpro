
import java.util.function.Function;
import java.util.function.IntFunction;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author leopa
 */
public class Area {
    
    public Area() {
        
    }
    
    Function<Double, Double> getAreaFromRadiusDouble = (radius) -> {
            return Math.PI * radius * radius;  
        };
    
    IntFunction<Double> getAreaFromRadiusInteger = (radius) -> {
            return Math.PI * radius * radius;
        };
    
    
}
