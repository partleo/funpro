

public class main {
    
    public static void main(String[] args) {
        
        Temp temp = new Temp();
        Area area = new Area();
        Dice dice = new Dice();
        ListItemCombiner combiner = new ListItemCombiner();
        
        int[] firstList = new int[]{1, 2, 3};
        int[] secondList = new int[]{3, 4};
        
        //1
        System.out.println(temp.toCelciusFromFahrenheitDouble.apply(100.0) 
                + "\n" + temp.toCelciusFromFahrenheitInteger.apply(100));
        System.out.println(area.getAreaFromRadiusDouble.apply(50.0) 
                + "\n" + area.getAreaFromRadiusInteger.apply(50));
        
        //2
        dice.rollDice();
        
        //3
        combiner.combine(firstList, secondList);
    }
}
