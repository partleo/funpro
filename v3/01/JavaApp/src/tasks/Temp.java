
import java.util.function.Function;
import java.util.function.IntFunction;


public class Temp {
    
    public Temp() {
        
    }
    
    public static double toDouble(int i){
        double d = i;
        return d;
    }
    
    Function<Double, Double> toCelciusFromFahrenheitDouble = (fahrenheit) -> {
            return (toDouble(5)/toDouble(9))*(fahrenheit-32);
        };
    
    IntFunction<Double> toCelciusFromFahrenheitInteger = (fahrenheit) -> {
            return (toDouble(5)/toDouble(9))*(fahrenheit-32);
        };
    
}
