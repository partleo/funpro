
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author leopa
 */
public class ListItemCombiner {
    
    public ListItemCombiner(){
        
    }
    
    public void combine(int[] firstList, int[] secondList){
        List<int[]> list = IntStream.of(firstList).parallel()
                .mapToObj(x -> IntStream.of(secondList).mapToObj(y -> new int[]{x, y}))
                .flatMap(element -> element)
                .collect(Collectors.toList());
        list.stream().map(element -> "{" + element[0] + "," + element[1] + "}").forEach(System.out::print);
    }
    
      
}
